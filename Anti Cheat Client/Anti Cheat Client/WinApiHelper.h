#pragma once
#include <Windows.h>
#include <TlHelp32.h>
#include <psapi.h>
#include <list>
#include <iostream>
#include <string>
#include <algorithm>

namespace WinApiHelper
{
	using namespace std;

	HANDLE getProcHandle(char* procName)
	{
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);

		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

		if (Process32First(snapshot, &entry) == TRUE)
		{
			while (Process32Next(snapshot, &entry) == TRUE)
			{
				if (strcmp(entry.szExeFile, procName) == 0)
				{
					HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);

					CloseHandle(snapshot);

					return hProcess;
				}
			}
		}

		return NULL;

		CloseHandle(snapshot);
	}

	/*
	The getModulesPaths can be used to detect 
	injections in 2 ways -
	1) Grab the loaded dlls exactly when the process starts and compare them against
	an updating list every X seconds
	2) Have a whitelist of DLLs for the process and compare them against the currently
	loaded dlls
	*/
	list<string> getModulesNames(HANDLE hProcess)
	{
		list<string> modulesNames;
		HMODULE lphModule[1024];
		DWORD cbNeeded;

		if (EnumProcessModules(hProcess, lphModule, sizeof(lphModule), &cbNeeded)) // get all loaded modules into 'lphModule' array
		{
			// loop through all loaded modules and append their name into our 'modulesNames' vector
			for (unsigned int i = 1; i < (cbNeeded / sizeof(HMODULE)); i++)
			{
				char moduleName[MAX_PATH];

				if (GetModuleFileNameEx(hProcess, lphModule[i], moduleName, sizeof(moduleName) / sizeof(char)))
				{
					string sModuleName = string(moduleName);
					sModuleName = sModuleName.substr(sModuleName.find_last_of('\\') + 1);

					transform(sModuleName.begin(), sModuleName.end(), sModuleName.begin(), tolower);

					modulesNames.push_back(sModuleName.substr(sModuleName.find_last_of('\\') + 1));
				}
			}
		}

		return modulesNames;
	}
}