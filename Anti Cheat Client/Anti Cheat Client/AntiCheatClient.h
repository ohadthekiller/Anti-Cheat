#pragma once
#include <iostream>
#include <string>
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <list>
#include "Helper.h"
#include "Protocol.h"
#include "Fingerprints.h"

#define PORT 8820


using namespace std;

class AntiCheatClient
{
public:
	AntiCheatClient();
	~AntiCheatClient();
	void ReportToServer(list<string> dlls);
	int signIn(string username, string password);
	int signUp(string username, string password);

private:
	WSADATA _WSAData;
	SOCKET _server;
	SOCKADDR_IN _addr;
};