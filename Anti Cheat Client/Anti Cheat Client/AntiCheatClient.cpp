#include "AntiCheatClient.h"


AntiCheatClient::AntiCheatClient()
{

	WSAStartup(MAKEWORD(2, 0), &this->_WSAData);
	this->_server = socket(AF_INET, SOCK_STREAM, 0);

	inet_pton(AF_INET, "127.0.0.1", &(this->_addr.sin_addr));

	this->_addr.sin_family = AF_INET;
	this->_addr.sin_port = htons(PORT);

	connect(this->_server, (SOCKADDR *)&this->_addr, sizeof(this->_addr));
	cout << "Connected to server!" << endl;
}

AntiCheatClient::~AntiCheatClient()
{
	closesocket(this->_server);
	WSACleanup();
	cout << "Socket closed." << endl << endl;
}


void AntiCheatClient::ReportToServer(list<string> dlls)
{
	string message = S_FOUND + Helper::getPaddedNumber(dlls.size(), 2);

	for (auto dllName : dlls)
	{
		message += Helper::getPaddedNumber(dllName.length(), 3) + dllName;
	}

	cout << message << endl;

	Helper::sendData(this->_server, message);
}

int AntiCheatClient::signIn(string username, string password)
{
	Helper::sendData(this->_server, S_SIGN_IN + Helper::getPaddedNumber(username.size(), 2) + username + Helper::getPaddedNumber(password.size(), 2) + password);

	if (Helper::getMessageTypeCode(this->_server) == SIGN_IN)
	{
		return Helper::getIntPartFromSocket(this->_server, 1);
	}

	return -1;
}

int AntiCheatClient::signUp(string username, string password)
{
	string macAddress = Fingerprints::getMacAddress();
	Helper::sendData(this->_server, S_SIGN_UP + Helper::getPaddedNumber(username.size(), 2) + username + Helper::getPaddedNumber(password.size(), 2) + password + Helper::getPaddedNumber(macAddress.size(), 2) + macAddress);

	if (Helper::getMessageTypeCode(this->_server) == SIGN_UP)
	{
		return Helper::getIntPartFromSocket(this->_server, 1);
	}

	return -1;
}