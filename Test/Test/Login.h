#pragma once

namespace Test {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;
	using namespace System::Net;
	using namespace System::Net::Sockets;

	/// <summary>
	/// Summary for Login
	/// </summary>
	public ref class Login : public System::Windows::Forms::Form
	{
	public:
		Login(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Login()
		{
			if (components)
			{
				delete components;
			}
		}
	private: bool drag;
	private: Point offset;
	private: System::Windows::Forms::Panel^  dragging;
	protected:
	private: System::Windows::Forms::Button^  Exit;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::RichTextBox^  Username;
	private: System::Windows::Forms::RichTextBox^  Password;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  Error_Message;
	private: System::Windows::Forms::Button^  Login_Button;

	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Panel^  panel2;
	private: TcpClient^ tcpClientC;





	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Login::typeid));
			this->dragging = (gcnew System::Windows::Forms::Panel());
			this->Exit = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->Username = (gcnew System::Windows::Forms::RichTextBox());
			this->Password = (gcnew System::Windows::Forms::RichTextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->Error_Message = (gcnew System::Windows::Forms::Label());
			this->Login_Button = (gcnew System::Windows::Forms::Button());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->dragging->SuspendLayout();
			this->panel1->SuspendLayout();
			this->panel2->SuspendLayout();
			this->SuspendLayout();
			// 
			// dragging
			// 
			this->dragging->Controls->Add(this->Exit);
			this->dragging->Location = System::Drawing::Point(0, 0);
			this->dragging->Name = L"dragging";
			this->dragging->Size = System::Drawing::Size(670, 43);
			this->dragging->TabIndex = 6;
			this->dragging->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &Login::dragging_MouseDown);
			this->dragging->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &Login::dragging_MouseMove);
			this->dragging->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &Login::dragging_MouseUp);
			// 
			// Exit
			// 
			this->Exit->BackColor = System::Drawing::Color::Transparent;
			this->Exit->Cursor = System::Windows::Forms::Cursors::Hand;
			this->Exit->FlatAppearance->MouseDownBackColor = System::Drawing::Color::Transparent;
			this->Exit->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Transparent;
			this->Exit->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->Exit->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(177)));
			this->Exit->ForeColor = System::Drawing::Color::Transparent;
			this->Exit->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Exit.Image")));
			this->Exit->Location = System::Drawing::Point(630, 3);
			this->Exit->Name = L"Exit";
			this->Exit->Size = System::Drawing::Size(39, 39);
			this->Exit->TabIndex = 4;
			this->Exit->UseVisualStyleBackColor = false;
			this->Exit->Click += gcnew System::EventHandler(this, &Login::Exit_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 36, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::SteelBlue;
			this->label1->Location = System::Drawing::Point(263, 68);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(142, 55);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Login";
			// 
			// Username
			// 
			this->Username->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->Username->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(177)));
			this->Username->ForeColor = System::Drawing::Color::Gray;
			this->Username->Location = System::Drawing::Point(0, 0);
			this->Username->Multiline = false;
			this->Username->Name = L"Username";
			this->Username->Size = System::Drawing::Size(287, 25);
			this->Username->TabIndex = 8;
			this->Username->Text = L"Username";
			this->Username->Enter += gcnew System::EventHandler(this, &Login::Username_Enter);
			this->Username->Leave += gcnew System::EventHandler(this, &Login::Username_Leave);
			// 
			// Password
			// 
			this->Password->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->Password->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(177)));
			this->Password->ForeColor = System::Drawing::Color::Gray;
			this->Password->Location = System::Drawing::Point(0, 0);
			this->Password->Multiline = false;
			this->Password->Name = L"Password";
			this->Password->Size = System::Drawing::Size(287, 25);
			this->Password->TabIndex = 9;
			this->Password->Text = L"Password";
			this->Password->Enter += gcnew System::EventHandler(this, &Login::Password_Enter);
			this->Password->Leave += gcnew System::EventHandler(this, &Login::Password_Leave);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Cursor = System::Windows::Forms::Cursors::Hand;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(177)));
			this->label2->Location = System::Drawing::Point(200, 329);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(269, 16);
			this->label2->TabIndex = 12;
			this->label2->Text = L"Don\'t have an account\? click here to register";
			this->label2->Click += gcnew System::EventHandler(this, &Login::label2_Click);
			// 
			// Error_Message
			// 
			this->Error_Message->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(177)));
			this->Error_Message->ForeColor = System::Drawing::Color::Red;
			this->Error_Message->Location = System::Drawing::Point(88, 231);
			this->Error_Message->Name = L"Error_Message";
			this->Error_Message->Size = System::Drawing::Size(493, 38);
			this->Error_Message->TabIndex = 11;
			this->Error_Message->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// Login_Button
			// 
			this->Login_Button->BackColor = System::Drawing::Color::SteelBlue;
			this->Login_Button->Cursor = System::Windows::Forms::Cursors::Hand;
			this->Login_Button->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->Login_Button->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(177)));
			this->Login_Button->ForeColor = System::Drawing::Color::White;
			this->Login_Button->Location = System::Drawing::Point(251, 272);
			this->Login_Button->Name = L"Login_Button";
			this->Login_Button->Size = System::Drawing::Size(165, 41);
			this->Login_Button->TabIndex = 10;
			this->Login_Button->Text = L"Login";
			this->Login_Button->UseVisualStyleBackColor = false;
			this->Login_Button->Click += gcnew System::EventHandler(this, &Login::Login_Button_Click);
			// 
			// panel1
			// 
			this->panel1->BackColor = System::Drawing::Color::SteelBlue;
			this->panel1->Controls->Add(this->Username);
			this->panel1->Location = System::Drawing::Point(191, 152);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(287, 28);
			this->panel1->TabIndex = 13;
			// 
			// panel2
			// 
			this->panel2->BackColor = System::Drawing::Color::SteelBlue;
			this->panel2->Controls->Add(this->Password);
			this->panel2->Location = System::Drawing::Point(191, 200);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(287, 28);
			this->panel2->TabIndex = 2;
			// 
			// Login
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::White;
			this->ClientSize = System::Drawing::Size(669, 380);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->Error_Message);
			this->Controls->Add(this->Login_Button);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->dragging);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Name = L"Login";
			this->Text = L"Login";
			this->dragging->ResumeLayout(false);
			this->panel1->ResumeLayout(false);
			this->panel2->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
private: System::Void Login_Button_Click(System::Object^  sender, System::EventArgs^  e) {
	if (Username->ForeColor == System::Drawing::Color::Gray
		|| Password->ForeColor == System::Drawing::Color::Gray)
	{
		Error_Message->Text = "Error! Please enter username and password.";
	}
	else
	{
		Error_Message->Text = "";

	}
}
private: System::Void Exit_Click(System::Object^  sender, System::EventArgs^  e) {
	this->Close();
}
private: System::Void Username_Enter(System::Object^  sender, System::EventArgs^  e) {
	if ((Username->Text->Trim() != "" || Username->Text != nullptr) && Username->ForeColor != System::Drawing::Color::Black)
	{
		Username->ForeColor = System::Drawing::Color::Black;
		Username->Text = "";
	}
}
private: System::Void Username_Leave(System::Object^  sender, System::EventArgs^  e) {
	if (Username->Text->Trim() == "" || Username->Text == nullptr)
	{
		Username->ForeColor = System::Drawing::Color::Gray;
		Username->Text = "Username";
	}
}
private: System::Void Password_Enter(System::Object^  sender, System::EventArgs^  e) {
	if ((Password->Text->Trim() != "" || Password->Text != nullptr) && Password->ForeColor != System::Drawing::Color::Black)
	{
		Password->ForeColor = System::Drawing::Color::Black;
		Password->Text = "";
	}
}
private: System::Void Password_Leave(System::Object^  sender, System::EventArgs^  e) {
	if (Password->Text->Trim() == "" || Password->Text == nullptr)
	{
		Password->ForeColor = System::Drawing::Color::Gray;
		Password->Text = "Password";
	}
}
private: System::Void label2_Click(System::Object^  sender, System::EventArgs^  e) {
	this->Close();
}
private: System::Void dragging_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	this->drag = true;
	this->offset = Point(e->X, e->Y);
}
private: System::Void dragging_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	if (this->drag) {
		Point currentScreenPos = PointToScreen(e->Location);
		Location = Point(currentScreenPos.X - this->offset.X, currentScreenPos.Y - this->offset.Y);
	}
}
private: System::Void dragging_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	this->drag = false;
}
};
}
