#pragma once

#include <string>


namespace Test {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for ControlPanel
	/// </summary>
	public ref class ControlPanel : public System::Windows::Forms::Form
	{
	public:
		ControlPanel(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~ControlPanel()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::Panel^  panel3;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  BannedPlayers;


	private: System::Windows::Forms::Button^  AllPlayers;
	private: System::Windows::Forms::DataGridView^  Data;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn5;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  dataGridViewTextBoxColumn6;
	private: System::Windows::Forms::Button^  Exit;


	private: bool dragging;
	private: Point offset;
	private: System::Windows::Forms::Button^  OnlinePlayers;

	


	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ControlPanel::typeid));
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->OnlinePlayers = (gcnew System::Windows::Forms::Button());
			this->AllPlayers = (gcnew System::Windows::Forms::Button());
			this->BannedPlayers = (gcnew System::Windows::Forms::Button());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->Exit = (gcnew System::Windows::Forms::Button());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->Data = (gcnew System::Windows::Forms::DataGridView());
			this->dataGridViewTextBoxColumn1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dataGridViewTextBoxColumn2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dataGridViewTextBoxColumn3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dataGridViewTextBoxColumn4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dataGridViewTextBoxColumn5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->dataGridViewTextBoxColumn6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->panel1->SuspendLayout();
			this->panel2->SuspendLayout();
			this->panel3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Data))->BeginInit();
			this->SuspendLayout();
			// 
			// panel1
			// 
			this->panel1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(39)), static_cast<System::Int32>(static_cast<System::Byte>(39)),
				static_cast<System::Int32>(static_cast<System::Byte>(39)));
			this->panel1->Controls->Add(this->OnlinePlayers);
			this->panel1->Controls->Add(this->AllPlayers);
			this->panel1->Controls->Add(this->BannedPlayers);
			this->panel1->Dock = System::Windows::Forms::DockStyle::Left;
			this->panel1->Location = System::Drawing::Point(0, 0);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(200, 584);
			this->panel1->TabIndex = 0;
			// 
			// OnlinePlayers
			// 
			this->OnlinePlayers->FlatAppearance->BorderSize = 0;
			this->OnlinePlayers->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->OnlinePlayers->Font = (gcnew System::Drawing::Font(L"Century Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->OnlinePlayers->ForeColor = System::Drawing::Color::White;
			this->OnlinePlayers->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"OnlinePlayers.Image")));
			this->OnlinePlayers->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->OnlinePlayers->Location = System::Drawing::Point(12, 184);
			this->OnlinePlayers->Name = L"OnlinePlayers";
			this->OnlinePlayers->Size = System::Drawing::Size(176, 59);
			this->OnlinePlayers->TabIndex = 2;
			this->OnlinePlayers->Text = L"Online Players";
			this->OnlinePlayers->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->OnlinePlayers->UseVisualStyleBackColor = true;
			this->OnlinePlayers->Click += gcnew System::EventHandler(this, &ControlPanel::OnlinePlayers_Click);
			// 
			// AllPlayers
			// 
			this->AllPlayers->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
			this->AllPlayers->FlatAppearance->BorderSize = 0;
			this->AllPlayers->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->AllPlayers->Font = (gcnew System::Drawing::Font(L"Century Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->AllPlayers->ForeColor = System::Drawing::Color::White;
			this->AllPlayers->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"AllPlayers.Image")));
			this->AllPlayers->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->AllPlayers->Location = System::Drawing::Point(12, 119);
			this->AllPlayers->Name = L"AllPlayers";
			this->AllPlayers->Size = System::Drawing::Size(176, 59);
			this->AllPlayers->TabIndex = 1;
			this->AllPlayers->Text = L"All Players";
			this->AllPlayers->UseVisualStyleBackColor = true;
			this->AllPlayers->Click += gcnew System::EventHandler(this, &ControlPanel::AllPlayers_Click);
			// 
			// BannedPlayers
			// 
			this->BannedPlayers->FlatAppearance->BorderSize = 0;
			this->BannedPlayers->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->BannedPlayers->Font = (gcnew System::Drawing::Font(L"Century Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->BannedPlayers->ForeColor = System::Drawing::Color::White;
			this->BannedPlayers->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"BannedPlayers.Image")));
			this->BannedPlayers->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->BannedPlayers->Location = System::Drawing::Point(12, 249);
			this->BannedPlayers->Name = L"BannedPlayers";
			this->BannedPlayers->Size = System::Drawing::Size(176, 59);
			this->BannedPlayers->TabIndex = 0;
			this->BannedPlayers->Text = L"Banned Players";
			this->BannedPlayers->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->BannedPlayers->UseVisualStyleBackColor = true;
			this->BannedPlayers->Click += gcnew System::EventHandler(this, &ControlPanel::BannedPlayers_Click);
			// 
			// panel2
			// 
			this->panel2->BackColor = System::Drawing::Color::MediumTurquoise;
			this->panel2->Controls->Add(this->Exit);
			this->panel2->Dock = System::Windows::Forms::DockStyle::Top;
			this->panel2->Location = System::Drawing::Point(200, 0);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(778, 38);
			this->panel2->TabIndex = 1;
			this->panel2->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &ControlPanel::panel2_MouseDown);
			this->panel2->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &ControlPanel::panel2_MouseMove);
			this->panel2->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &ControlPanel::panel2_MouseUp);
			// 
			// Exit
			// 
			this->Exit->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->Exit->Cursor = System::Windows::Forms::Cursors::Hand;
			this->Exit->FlatAppearance->BorderSize = 0;
			this->Exit->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->Exit->Font = (gcnew System::Drawing::Font(L"Century Gothic", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->Exit->ForeColor = System::Drawing::Color::White;
			this->Exit->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"Exit.Image")));
			this->Exit->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->Exit->Location = System::Drawing::Point(738, 1);
			this->Exit->Name = L"Exit";
			this->Exit->Size = System::Drawing::Size(40, 38);
			this->Exit->TabIndex = 2;
			this->Exit->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->Exit->UseVisualStyleBackColor = true;
			this->Exit->Click += gcnew System::EventHandler(this, &ControlPanel::Exit_Click);
			// 
			// panel3
			// 
			this->panel3->BackColor = System::Drawing::Color::MediumTurquoise;
			this->panel3->Controls->Add(this->label1);
			this->panel3->Location = System::Drawing::Point(248, 1);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(217, 64);
			this->panel3->TabIndex = 2;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Kristen ITC", 21.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->ForeColor = System::Drawing::Color::White;
			this->label1->Location = System::Drawing::Point(3, 12);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(215, 40);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Control Panel";
			// 
			// Data
			// 
			this->Data->AutoSizeColumnsMode = System::Windows::Forms::DataGridViewAutoSizeColumnsMode::Fill;
			this->Data->BackgroundColor = System::Drawing::Color::LightCyan;
			this->Data->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(177)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->Data->DefaultCellStyle = dataGridViewCellStyle1;
			this->Data->Location = System::Drawing::Point(248, 119);
			this->Data->Name = L"Data";
			this->Data->ReadOnly = true;
			this->Data->Size = System::Drawing::Size(689, 414);
			this->Data->TabIndex = 2;
			// 
			// dataGridViewTextBoxColumn1
			// 
			this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
			// 
			// dataGridViewTextBoxColumn2
			// 
			this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
			// 
			// dataGridViewTextBoxColumn3
			// 
			this->dataGridViewTextBoxColumn3->Name = L"dataGridViewTextBoxColumn3";
			// 
			// dataGridViewTextBoxColumn4
			// 
			this->dataGridViewTextBoxColumn4->Name = L"dataGridViewTextBoxColumn4";
			// 
			// dataGridViewTextBoxColumn5
			// 
			this->dataGridViewTextBoxColumn5->Name = L"dataGridViewTextBoxColumn5";
			// 
			// dataGridViewTextBoxColumn6
			// 
			this->dataGridViewTextBoxColumn6->Name = L"dataGridViewTextBoxColumn6";
			// 
			// ControlPanel
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::White;
			this->ClientSize = System::Drawing::Size(978, 584);
			this->Controls->Add(this->Data);
			this->Controls->Add(this->panel3);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Margin = System::Windows::Forms::Padding(3, 4, 3, 4);
			this->Name = L"ControlPanel";
			this->Text = L"Control Panel";
			this->Load += gcnew System::EventHandler(this, &ControlPanel::ControlPanel_Load);
			this->panel1->ResumeLayout(false);
			this->panel2->ResumeLayout(false);
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->Data))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void ControlPanel_Load(System::Object^  sender, System::EventArgs^  e)
	{
		this->dragging = false;
	}
	private: System::Void AllPlayers_Click(System::Object^  sender, System::EventArgs^  e)
	{
		this->Data->Columns->Clear();

		this->Data->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
			this->dataGridViewTextBoxColumn1,
				this->dataGridViewTextBoxColumn2, this->dataGridViewTextBoxColumn3, this->dataGridViewTextBoxColumn4,
				this->dataGridViewTextBoxColumn5, this->dataGridViewTextBoxColumn6
		});
		// 
		// dataGridViewTextBoxColumn1
		// 
		this->dataGridViewTextBoxColumn1->HeaderText = L"Id";
		this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
		// 
		// dataGridViewTextBoxColumn2
		// 
		this->dataGridViewTextBoxColumn2->HeaderText = L"Username";
		this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
		// 
		// dataGridViewTextBoxColumn3
		// 
		this->dataGridViewTextBoxColumn3->HeaderText = L"Password";
		this->dataGridViewTextBoxColumn3->Name = L"dataGridViewTextBoxColumn3";
		// 
		// dataGridViewTextBoxColumn4
		// 
		this->dataGridViewTextBoxColumn4->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		this->dataGridViewTextBoxColumn4->HeaderText = L"Fingerprint";
		this->dataGridViewTextBoxColumn4->Name = L"dataGridViewTextBoxColumn4";
		// 
		// dataGridViewTextBoxColumn5
		// 
		this->dataGridViewTextBoxColumn5->HeaderText = L"Is Online";
		this->dataGridViewTextBoxColumn5->Name = L"dataGridViewTextBoxColumn5";
		// 
		// dataGridViewTextBoxColumn6
		// 
		this->dataGridViewTextBoxColumn6->HeaderText = L"Is Banned";
		this->dataGridViewTextBoxColumn6->Name = L"dataGridViewTextBoxColumn6";
		//

	}
	private: System::Void BannedPlayers_Click(System::Object^  sender, System::EventArgs^  e)
	{
		this->Data->Columns->Clear();

		this->Data->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
			this->dataGridViewTextBoxColumn1,
				this->dataGridViewTextBoxColumn2, this->dataGridViewTextBoxColumn3, this->dataGridViewTextBoxColumn4,
				this->dataGridViewTextBoxColumn5, this->dataGridViewTextBoxColumn6
		});
		// 
		// dataGridViewTextBoxColumn1
		// 
		this->dataGridViewTextBoxColumn1->HeaderText = L"Id";
		this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
		// 
		// dataGridViewTextBoxColumn2
		// 
		this->dataGridViewTextBoxColumn2->HeaderText = L"Username";
		this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
		// 
		// dataGridViewTextBoxColumn3
		// 
		this->dataGridViewTextBoxColumn3->HeaderText = L"Password";
		this->dataGridViewTextBoxColumn3->Name = L"dataGridViewTextBoxColumn3";
		// 
		// dataGridViewTextBoxColumn4
		// 
		this->dataGridViewTextBoxColumn4->HeaderText = L"Fingerprint";
		this->dataGridViewTextBoxColumn4->Name = L"dataGridViewTextBoxColumn4";
		// 
		// dataGridViewTextBoxColumn5
		// 
		this->dataGridViewTextBoxColumn5->HeaderText = L"Ban Date";
		this->dataGridViewTextBoxColumn5->Name = L"dataGridViewTextBoxColumn5";
		// 
		// dataGridViewTextBoxColumn6
		// 
		this->dataGridViewTextBoxColumn6->HeaderText = L"Ban Reason";
		this->dataGridViewTextBoxColumn6->Name = L"dataGridViewTextBoxColumn6";
		//
		PrintToTable("AAA,AAB,AAC;ABA,ABB,ABC;ACA,ACB,ACC;ADA,ADB,ADC;ADA,ADB,ADC;");
	}
	
	private: System::Void OnlinePlayers_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Data->Columns->Clear();

		this->Data->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {
			this->dataGridViewTextBoxColumn1,
				this->dataGridViewTextBoxColumn2, this->dataGridViewTextBoxColumn3, this->dataGridViewTextBoxColumn4
		});
		// 
		// dataGridViewTextBoxColumn1
		// 
		this->dataGridViewTextBoxColumn1->HeaderText = L"Id";
		this->dataGridViewTextBoxColumn1->Name = L"dataGridViewTextBoxColumn1";
		// 
		// dataGridViewTextBoxColumn2
		// 
		this->dataGridViewTextBoxColumn2->HeaderText = L"Username";
		this->dataGridViewTextBoxColumn2->Name = L"dataGridViewTextBoxColumn2";
		// 
		// dataGridViewTextBoxColumn3
		// 
		this->dataGridViewTextBoxColumn3->HeaderText = L"Password";
		this->dataGridViewTextBoxColumn3->Name = L"dataGridViewTextBoxColumn3";
		// 
		// dataGridViewTextBoxColumn4
		// 
		this->dataGridViewTextBoxColumn4->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
		this->dataGridViewTextBoxColumn4->HeaderText = L"Fingerprint";
		this->dataGridViewTextBoxColumn4->Name = L"dataGridViewTextBoxColumn4";
		// 
	}

	private: System::Void Exit_Click(System::Object^  sender, System::EventArgs^  e)
	{
		Close();
	}

	private: System::Void panel2_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		//tell the form its gonna be draggin'
		this->dragging = true;
		this->offset = Point(e->X, e->Y);
	}
	private: System::Void panel2_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		if (this->dragging) {
			Point currentScreenPos = PointToScreen(e->Location);
			Location = Point(currentScreenPos.X - this->offset.X, currentScreenPos.Y - this->offset.Y);
		}
	}
	private: System::Void panel2_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
		this->dragging = false;
	}


	private: void ControlPanel::PrintToTable(std::string info)
	{
		int rowIndex = 0;
		int cellIndex = 0;
		cli::array<String^>^ sentences = (gcnew String(info.c_str()))->Split(';');
		for(int i = 0; i < sentences->Length - 2; ++i)
		{
			Data->Rows->Add();
		}
		for each (String^ sentence in sentences)
		{
			cli::array<String^>^ words = sentence->Split(',');
			for each (String^ word in words) {
				if (word != "")
				{
					Data->Rows[rowIndex]->Cells[cellIndex]->Value = word;
					cellIndex++;
				}
			}
			rowIndex++;
			cellIndex = 0;
		}
		/*String^ str = "";
		int row = 0;
		for (int i = 0; i < info.length(); i++)
		{
			if (info[i] == ',')
			{
				if (Data->RowCount < row + 1)
				{
					Data->Rows->Add();
				}
				this->Data->Rows[row]->Cells[1]->Value = str;
				row++;
				str = "";
			}
			else
			{
				str += info[i];
			}
		}*/
	}
#pragma endregion
};

}
