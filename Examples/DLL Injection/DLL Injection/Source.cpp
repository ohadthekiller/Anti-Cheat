#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>

HANDLE getProcHandle(char* procName)
{
	PROCESSENTRY32 entry;
	entry.dwSize = sizeof(PROCESSENTRY32);

	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);

	if (Process32First(snapshot, &entry) == TRUE)
	{
		while (Process32Next(snapshot, &entry) == TRUE)
		{
			if (strcmp(entry.szExeFile, procName) == 0)
			{
				HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);

				CloseHandle(snapshot);

				return hProcess;
			}
		}
	}

	CloseHandle(snapshot);
}

bool InjectDLL(char* procName, char* dllPath)
{
	char DLL_NAME[MAX_PATH] = { 0 };
	strncpy_s(DLL_NAME, dllPath, MAX_PATH - 1);

	HANDLE hProc = 0;
	LPVOID RemoteString, LoadLibAddy;

	if (!(hProc = getProcHandle(procName))) return false;

	// Get the address of LoadLibraryA function, which is used to Load a DLL
	LoadLibAddy = (LPVOID)GetProcAddress(GetModuleHandle("kernel32.dll"), "LoadLibraryA");

	// Reserve space to write our dllPath in the target process memory
	RemoteString = (LPVOID)VirtualAllocEx(hProc, NULL, strlen(DLL_NAME), MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

	// Write our dllPath in the target process memory
	WriteProcessMemory(hProc, (LPVOID)RemoteString, DLL_NAME, strlen(DLL_NAME), NULL);

	// Call function LoadLibraryA from inside the target process, and load the dll
	CreateRemoteThread(hProc, NULL, NULL, (LPTHREAD_START_ROUTINE)LoadLibAddy, (LPVOID)RemoteString, NULL, NULL);

	// Close the handle to the process
	CloseHandle(hProc);
	return true;
}

void main()
{
	if (InjectDLL("VictimProcess.exe", "C:\\Users\\User\\Documents\\Visual Studio 2013\\Projects\\Anti Cheat Client\\Anti-Cheat\\Examples\\DLL Injection\\Release\\DLL.dll"))
	{
		std::cout << "DLL has been injected successfuly" << std::endl;
	}
	else
	{
		std::cout << "The target process wasn't found" << std::endl;
	}

	system("pause");
}