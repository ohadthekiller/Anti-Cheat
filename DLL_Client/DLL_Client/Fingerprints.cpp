#include "Fingerprints.h"

namespace Fingerprints
{
	std::string Fingerprints::getMacAddress()
	{
		char macAddress[18] = { 0 };
		IP_ADAPTER_INFO AdapterInfo[10];

		DWORD dwBufLen = sizeof(AdapterInfo);

		DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);

		if (dwStatus != ERROR_SUCCESS)
		{
			return "";
		}

		PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;

		if (pAdapterInfo)
		{
			_snprintf_s(macAddress, 18, "%x:%x:%x:%x:%x:%x",
				pAdapterInfo->Address[0], pAdapterInfo->Address[1],
				pAdapterInfo->Address[2], pAdapterInfo->Address[3],
				pAdapterInfo->Address[4], pAdapterInfo->Address[5]);
		}
		else
		{
			return "";
		}

		return std::string(macAddress);

	}
}