#include "AntiCheatClient.h"
#include "WinApiHelper.h"
#include <list>
#include <string>

using namespace std;


// Example function - proof of concept...
void detectDllInjection(AntiCheatClient& client)
{
	HANDLE hProc = WinApiHelper::getProcHandle("VictimProcess.exe");
	list<string> oldModules = WinApiHelper::getModulesNames(hProc);

	client.ReportToServer(oldModules);
	
	while (true)
	{
		list<string> recentModules = WinApiHelper::getModulesNames(hProc);

		list<string> newModules;
		for (auto moduleName : recentModules)
		{
			if (oldModules.end() == find(oldModules.begin(), oldModules.end(), moduleName))
			{
				oldModules.push_back(moduleName);
				newModules.push_back(moduleName);
			}
		}

		if (newModules.size() > 0)
		{
			client.ReportToServer(newModules);
		}

		Sleep(1000);
	}
}

bool trySignIn(AntiCheatClient& client)
{
	string username, password;

	cout << "Enter username: ";
	getline(cin, username, '\n');
	cout << "Enter password: ";
	getline(cin, password, '\n');

	switch (client.signIn(username, password))
	{
	case 0:
		cout << "Connected successfuly!" << endl;
		return true;
	case 1:
		cout << "Username and password don't match!" << endl;
		break;
	case 2:
		cout << "User is already connected!" << endl;
		break;
	default:
		cout << "Sign in error!" << endl;
	}

	return false;
}

void trySignUp(AntiCheatClient &client)
{
	string username, password;

	cout << "Enter username: ";
	getline(cin, username, '\n');
	cout << "Enter password: ";
	getline(cin, password, '\n');

	switch (client.signUp(username, password))
	{
	case 0:
		cout << "Signed up successfuly!" << endl;
		break;
	case 1:
		cout << "Invalid username!" << endl;
		break;
	case 2:
		cout << "Invalid password!" << endl;
		break;
	case 3:
		cout << "User already exists!" << endl;
		break;
	case 4:
		cout << "Database error!" << endl;
		break;
	default:
		cout << "Sign up error!" << endl;
	}
}
/*void main()
{
	AntiCheatClient client;
	int choice;

	do
	{
		cout << "1) Sign in with an existing account" << endl;
		cout << "2) Sign up and make a new account" << endl;
		cout << "3) Exit" << endl;

		cout << "Enter your choice: ";
		cin >> choice;
		cin.ignore();

		switch (choice)
		{
		case 1:
		{
			if (trySignIn(client))
			{
				detectDllInjection(client);
			}
			break;
		}
		case 2:
			trySignUp(client);
			break;
		case 3:
			cout << "Goodbye!" << endl;
			break;
		default:
			cout << "Please enter a valid option" << endl;
		}
	} while (choice != 3);

	system("pause");
}*/