#pragma once

#include "AntiCheatClient.h"
#include <thread>

AntiCheatClient* client;

extern "C"
{
	extern __declspec(dllexport) string tryLogin(string username, string password)
	{
		switch (client->signIn(username, password))
		{
		case 0:
			return "Connected successfuly";
			break;
		case 1:
			return "Username and password don't match!";
			break;
		case 2:
			return "User is already connected!";
			break;
		default:
			return "Sign in error!";
		}
	}

	extern __declspec(dllexport) string trySignUp(string username, string password)
	{
		switch (client->signUp(username, password))
		{
		case 0:
			return "Signed up successfuly!";
			break;
		case 1:
			return "Invalid username!";
			break;
		case 2:
			return "Invalid password!";
			break;
		case 3:
			return "User already exists!";
			break;
		case 4:
			return "Database error!";
			break;
		default:
			return "Sign up error!";
		}
	}
}