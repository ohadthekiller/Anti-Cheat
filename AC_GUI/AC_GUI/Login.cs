﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AC_GUI
{
    public partial class Login : Form
    {
        public enum login_errors : int
        {
            Connected,
            DontMatch,
            AlreadyConnected,
            AnotherError
        }


        [DllImport("Client.dll")]
        extern static void initClient();

        [DllImport("Client.dll")]
        extern static void detectDllInjection();

        [DllImport("Client.dll")]
        extern static int LoginFunction(string username, string password);

        private bool drag;
        private Point offset;
        //private Thread t;


        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            initClient();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void dragging_MouseDown(object sender, MouseEventArgs e)
        {
            this.drag = true;
            this.offset = new Point(e.X, e.Y);
        }

        private void dragging_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X - this.offset.X, currentScreenPos.Y - this.offset.Y);
            }
        }

        private void dragging_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void Username_Enter(object sender, EventArgs e)
        {
            if ((Username.Text.Trim() != "" || Username.Text != null) && Username.ForeColor != System.Drawing.Color.Black)
            {
                Username.ForeColor = System.Drawing.Color.Black;
                Username.Text = "";
            }
        }

        private void Username_Leave(object sender, EventArgs e)
        {
            if (Username.Text.Trim() == "" || Username.Text == null)
            {
                Username.ForeColor = System.Drawing.Color.Gray;
                Username.Text = "Username";
            }
        }

        private void Password_Enter(object sender, EventArgs e)
        {
            if ((Password.Text.Trim() != "" || Password.Text != null) && Password.ForeColor != System.Drawing.Color.Black)
            {
                Password.ForeColor = System.Drawing.Color.Black;
                Password.Text = "";
            }
        }

        private void Password_Leave(object sender, EventArgs e)
        {
            if (Password.Text.Trim() == "" || Password.Text == null)
            {
                Password.ForeColor = System.Drawing.Color.Gray;
                Password.Text = "Password";
            }
        }

        private void Register_Button_Click(object sender, EventArgs e)
        {
            if (Username.ForeColor == System.Drawing.Color.Gray
                || Password.ForeColor == System.Drawing.Color.Gray)
            {
                Error_Message.Text = "Error! Please enter username and password.";
            }
            else
            {
                switch (LoginFunction(this.Username.Text, this.Password.Text))
                {
                    case (int)login_errors.Connected:
                        {
                            Error_Message.Text = "";
                            break;
                        }
                    case (int)login_errors.DontMatch:
                        {
                            Error_Message.Text = "Username and Password don't match, please try again!";
                            break;
                        }
                    case (int)login_errors.AlreadyConnected:
                        {
                            Error_Message.Text = "This user is already connected!";
                            break;
                        }
                    case (int)login_errors.AnotherError:
                        {
                            Error_Message.Text = "Server error, please try login later.";
                            break;
                        }
                    default:
                        {
                            Error_Message.Text = "Error, please try login later.";
                            break;
                        }
                }
            }

        }
    }
}
