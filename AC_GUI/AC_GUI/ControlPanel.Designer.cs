﻿namespace AC_GUI
{
    partial class ControlPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlPanel));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.OnlinePlayers = new System.Windows.Forms.Button();
            this.AllPlayers = new System.Windows.Forms.Button();
            this.BannedPlayers = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Data = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Data)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(39)))), ((int)(((byte)(39)))));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.OnlinePlayers);
            this.panel1.Controls.Add(this.AllPlayers);
            this.panel1.Controls.Add(this.BannedPlayers);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 584);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(159, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "Control Panel";
            // 
            // OnlinePlayers
            // 
            this.OnlinePlayers.FlatAppearance.BorderSize = 0;
            this.OnlinePlayers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.OnlinePlayers.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OnlinePlayers.ForeColor = System.Drawing.Color.White;
            this.OnlinePlayers.Image = ((System.Drawing.Image)(resources.GetObject("OnlinePlayers.Image")));
            this.OnlinePlayers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.OnlinePlayers.Location = new System.Drawing.Point(12, 184);
            this.OnlinePlayers.Name = "OnlinePlayers";
            this.OnlinePlayers.Size = new System.Drawing.Size(176, 59);
            this.OnlinePlayers.TabIndex = 2;
            this.OnlinePlayers.Text = "Online Players";
            this.OnlinePlayers.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.OnlinePlayers.UseVisualStyleBackColor = true;
            this.OnlinePlayers.Click += new System.EventHandler(this.OnlinePlayers_Click);
            // 
            // AllPlayers
            // 
            this.AllPlayers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.AllPlayers.FlatAppearance.BorderSize = 0;
            this.AllPlayers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AllPlayers.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AllPlayers.ForeColor = System.Drawing.Color.White;
            this.AllPlayers.Image = ((System.Drawing.Image)(resources.GetObject("AllPlayers.Image")));
            this.AllPlayers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.AllPlayers.Location = new System.Drawing.Point(12, 119);
            this.AllPlayers.Name = "AllPlayers";
            this.AllPlayers.Size = new System.Drawing.Size(176, 59);
            this.AllPlayers.TabIndex = 1;
            this.AllPlayers.Text = "All Players";
            this.AllPlayers.UseVisualStyleBackColor = true;
            this.AllPlayers.Click += new System.EventHandler(this.AllPlayers_Click);
            // 
            // BannedPlayers
            // 
            this.BannedPlayers.FlatAppearance.BorderSize = 0;
            this.BannedPlayers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BannedPlayers.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BannedPlayers.ForeColor = System.Drawing.Color.White;
            this.BannedPlayers.Image = ((System.Drawing.Image)(resources.GetObject("BannedPlayers.Image")));
            this.BannedPlayers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BannedPlayers.Location = new System.Drawing.Point(12, 249);
            this.BannedPlayers.Name = "BannedPlayers";
            this.BannedPlayers.Size = new System.Drawing.Size(176, 59);
            this.BannedPlayers.TabIndex = 0;
            this.BannedPlayers.Text = "Banned Players";
            this.BannedPlayers.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BannedPlayers.UseVisualStyleBackColor = true;
            this.BannedPlayers.Click += new System.EventHandler(this.BannedPlayers_Click);
            // 
            // Exit
            // 
            this.Exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Exit.FlatAppearance.BorderSize = 0;
            this.Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Exit.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exit.ForeColor = System.Drawing.Color.White;
            this.Exit.Image = ((System.Drawing.Image)(resources.GetObject("Exit.Image")));
            this.Exit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Exit.Location = new System.Drawing.Point(738, 0);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(40, 38);
            this.Exit.TabIndex = 2;
            this.Exit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumTurquoise;
            this.panel2.Controls.Add(this.Exit);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(200, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(778, 38);
            this.panel2.TabIndex = 3;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseDown);
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseMove);
            this.panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseUp);
            // 
            // Data
            // 
            this.Data.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.Data.BackgroundColor = System.Drawing.Color.White;
            this.Data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Data.Cursor = System.Windows.Forms.Cursors.Default;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Data.DefaultCellStyle = dataGridViewCellStyle1;
            this.Data.Location = new System.Drawing.Point(248, 119);
            this.Data.Name = "Data";
            this.Data.ReadOnly = true;
            this.Data.Size = new System.Drawing.Size(689, 414);
            this.Data.TabIndex = 4;
            // 
            // ControlPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(978, 584);
            this.Controls.Add(this.Data);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ControlPanel";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Data)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button OnlinePlayers;
        private System.Windows.Forms.Button AllPlayers;
        private System.Windows.Forms.Button BannedPlayers;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView Data;
    }
}

