﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AC_GUI
{
    public partial class ControlPanel : Form
    {
        [DllImport("Server.dll")]
        extern static void initServer();

        [DllImport("Server.dll")]
        extern static string getAllUsers();

        [DllImport("Server.dll")]
        extern static string getOnlineUsers();

        private bool drag;
        private Point offset;
        private Thread t;

        public ControlPanel()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
            Environment.Exit(0);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.t = new Thread(initServer);
            t.Start();
            Thread.Sleep(500);
        }

        private void panel2_MouseDown(object sender, MouseEventArgs e)
        {
            this.drag = true;
            this.offset = new Point(e.X, e.Y);
        }

        private void panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.drag == true)
            {
                Point currentLocation = PointToScreen(e.Location);
                Location = new Point(currentLocation.X - this.offset.X, currentLocation.Y - this.offset.Y);
            }
        }

        private void panel2_MouseUp(object sender, MouseEventArgs e)
        {
            this.drag = false;
        }

        private void AllPlayers_Click(object sender, EventArgs e)
        {
            this.Data.Columns.Clear();
            this.Data.ColumnCount = 3;
            this.Data.Columns[0].Name = "Username";
            this.Data.Columns[1].Name = "Password";
            this.Data.Columns[2].Name = "Fingerprint";
            PrintToTable(getAllUsers());
        }

        private void OnlinePlayers_Click(object sender, EventArgs e)
        {
            this.Data.Columns.Clear();
            this.Data.ColumnCount = 3;
            this.Data.Columns[0].Name = "Username";
            this.Data.Columns[1].Name = "Password";
            this.Data.Columns[2].Name = "Fingerprint";
            PrintToTable(getOnlineUsers());
        }

        private void BannedPlayers_Click(object sender, EventArgs e)
        {
            this.Data.Columns.Clear();
            this.Data.ColumnCount = 3;
            this.Data.Columns[0].Name = "Username";
            this.Data.Columns[1].Name = "Password";
            this.Data.Columns[2].Name = "Fingerprint";
            PrintToTable(getAllUsers());
        }

        private void PrintToTable(string info)
        {
            int rowIndex = 0;
            int cellIndex = 0;
            string[] sentences = info.Split(';');
            for (int i = 0; i < sentences.Length - 2; ++i)
            {
                Data.Rows.Add();
            }
            foreach (String sentence in sentences)

            {
                string[] words = sentence.Split(',');
                foreach (string word in words)
                {
                    if (word != "")
                    {
                        Data.Rows[rowIndex].Cells[cellIndex].Value = word;
                        cellIndex++;
                    }
                }
                rowIndex++;
                cellIndex = 0;
            }
        }

    }
}
