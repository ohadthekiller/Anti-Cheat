#pragma once

#include "AntiCheatClient.h"
#include <thread>
#include "WinApiHelper.h"


AntiCheatClient* myClient;

enum class login_errors : int
{
	Connected,
	DontMatch,
	AlreadyConnected,
	AnotherError
};

enum class register_errors : int
{
	Registered,
	InvalidUsername,
	InvalidPassword,
	UsernameAlreadyExist,
	DatabaseError,
	AnotherError
};

void detectDllInjection(AntiCheatClient& client)
{
	HANDLE hProc = WinApiHelper::getProcHandle("VictimProcess.exe");
	list<string> oldModules = WinApiHelper::getModulesNames(hProc);

	client.ReportToServer(oldModules);

	while (true)
	{
		list<string> recentModules = WinApiHelper::getModulesNames(hProc);

		list<string> newModules;
		for (auto moduleName : recentModules)
		{
			if (oldModules.end() == find(oldModules.begin(), oldModules.end(), moduleName))
			{
				oldModules.push_back(moduleName);
				newModules.push_back(moduleName);
			}
		}

		if (newModules.size() > 0)
		{
			client.ReportToServer(newModules);
		}

		Sleep(1000);
	}
}

extern "C"
{
	extern __declspec(dllexport) int LoginFunction(const char* username, const char* password)
	{
		// get mac address / fingerprint
		// send to server
		// check response
			// if successful, start detection function
		// return response (int code)
		switch (myClient->signIn(username, password))
		{
		case 0:
		{
			thread t = thread(detectDllInjection, *myClient);
			t.detach();
			return static_cast<int>(login_errors::Connected);
		}
		case 1:
			return static_cast<int>(login_errors::DontMatch);
		case 2:
			return static_cast<int>(login_errors::AlreadyConnected);
		default:
			return static_cast<int>(login_errors::AnotherError);
		}
	}

	extern __declspec(dllexport) int RegisterFunction(const char* username, const char* password)
	{
		switch (myClient->signUp(username, password))
		{
		case 0:
			return static_cast<int>(register_errors::Registered);
			break;
		case 1:
			static_cast<int>(register_errors::InvalidUsername);
			break;
		case 2:
			static_cast<int>(register_errors::InvalidPassword);
			break;
		case 3:
			static_cast<int>(register_errors::UsernameAlreadyExist);
			break;
		case 4:
			static_cast<int>(register_errors::DatabaseError);
			break;
		default:
			static_cast<int>(register_errors::AnotherError);
		}
	}


	extern __declspec(dllexport) void initClient()
	{
		myClient = new AntiCheatClient();
	}


	extern __declspec(dllexport) string getFingerprint()
	{
		return Fingerprints::getMacAddress();
	}

	/*extern __declspec(dllexport) bool initServer()
	{
		WSAInitializer wsaInit;
		myServer = new Server();
		myServer->serve();

		return true;
	}

	extern __declspec(dllexport) char* getAllUsers()
	{
		string users = myServer->getAllUsers();
		char* ret = new char[users.size() + 1];
		memcpy(ret, users.c_str(), users.size());
		ret[users.size()] = 0;
		return ret;
	}

	extern __declspec(dllexport) char* getOnlineUsers()
	{
		string users = myServer->getOnlineUsers();
		char* ret = new char[users.size() + 1];
		memcpy(ret, &users[0], users.size());
		ret[users.size()] = 0;
		return ret;
	}*/
}