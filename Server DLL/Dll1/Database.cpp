#include "DataBase.h"

int resultCount;

DataBase::DataBase()
{
	if (sqlite3_open("AntiCheatUsers.db", &this->_db))
	{
		throw;
	}
	
	char* zErrMsg = 0;

	int rc = sqlite3_exec(this->_db, "create table if not exists ac_users(username text primary key not null, password text not null, macaddress text not null);", NULL, NULL, &zErrMsg);

	if (rc)
	{
		throw;
	}
}

DataBase::~DataBase()
{
	sqlite3_close(this->_db);
}

bool DataBase::isUserExists(string username)
{
	char* zErrMsg = 0;
	string query = "select username from ac_users where username='" + username + "';";

	int rc = sqlite3_exec(this->_db, query.c_str(), callbackCount, NULL, &zErrMsg);

	return resultCount == 0 ? false : true;
}

bool DataBase::addNewUser(string username, string password, string macAddress)
{
	int rc;
	char* zErrMsg = 0;
	string query = "insert into ac_users values('" + username + "', '" + password + "', " + macAddress + "');";

	rc = sqlite3_exec(this->_db, query.c_str(), NULL, NULL, &zErrMsg);
	if (rc)
	{
		return false;
	}

	return true;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	char* zErrMsg = 0;
	resultCount = 0;
	string query = "select username from ac_users where username='" + username + "' and password='" + password + "';";

	sqlite3_exec(this->_db, query.c_str(), callbackCount, NULL, &zErrMsg);

	return resultCount == 0 ? false : true;
}

std::string DataBase::getAllUsers()
{
	char* zErrMsg = 0;
	string result;
	const char* query = "select username, password, macaddress from ac_users;";

	sqlite3_exec(this->_db, query, callbackLine, &result, &zErrMsg);

	return result;
}

int DataBase::callbackCount(void* unused, int argc, char** argv, char** columns)
{
	resultCount = argc;
	string str = argv[0];
	return 0;
}

int DataBase::callbackLine(void* resString, int argc, char** argv, char** columns)
{
	if (argc > 0)
	{
		for (unsigned int i = 0; i < argc; i++)
		{
			*static_cast<string*>(resString) += argv[i];

			if (i != argc - 1)
			{
				*static_cast<string*>(resString) += ",";
			}
		}

		*static_cast<string*>(resString) += ";";
	}

	return 0;
}