#pragma once

#include "WSAInitializer.h"
#include "Server.h"
#include <thread>

Server* myServer;

extern "C"
{
	
	extern __declspec(dllexport) void initServer()
	{
		WSAInitializer wsaInit;
		myServer = new Server();
		myServer->serve();
	}

	extern __declspec(dllexport) char* getAllUsers()
	{
		string users = myServer->getAllUsers();
		char* ret = new char[users.size() + 1];
		memcpy(ret, users.c_str(), users.size());
		ret[users.size()] = 0;
		return ret;
	}

	extern __declspec(dllexport) char* getOnlineUsers()
	{
		string users = myServer->getOnlineUsers();
		char* ret = new char[users.size() + 1];
		memcpy(ret, &users[0], users.size());
		ret[users.size()] = 0;
		return ret;
	}
}