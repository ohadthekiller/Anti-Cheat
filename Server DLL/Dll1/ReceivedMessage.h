#pragma once
#include <string>
#include <vector>
#include <WinSock2.h>

using namespace std;

class ReceivedMessage
{
public:
	ReceivedMessage(SOCKET sock, int messageCode);//build a message from client by socket and message code
	ReceivedMessage(SOCKET sock, int messageCode, vector<string> values);//build a message from client by socket and message code and values from this message
	SOCKET getSock();//return socket of this message
	int getMessageCode();//return this message code
	vector<string>& getValues();//return this values from the message
private:
	SOCKET _sock;
	int _messageCode;
	vector<string> _values;
};