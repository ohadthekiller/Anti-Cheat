#include "User.h"


User::User(string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
}


void User::send(string message)
{
	Helper::sendData(this->_sock, message);
}

string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}