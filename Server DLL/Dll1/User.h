#pragma once

#include <iostream>
#include "Helper.h"

using namespace std;

class User
{
public:
	User(string username, SOCKET sock);
	void send(string message);//send this user message using socket
	string getUsername();//return string of this username
	SOCKET getSocket();//return socket of this user

private:
	string _username;
	SOCKET _sock;
};