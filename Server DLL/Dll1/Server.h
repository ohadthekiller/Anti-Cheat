#pragma once
#include "DataBase.h"
#include <iostream>
#include <map>
#include <mutex>
#include <queue>
#include <condition_variable>
#include <exception>
#include <thread>
#include <WinSock2.h>
#include <algorithm>
#include "WhitelistedModules.h"
#include "ReceivedMessage.h"
#include "Helper.h"
#include "Protocol.h"
#include "User.h"
#include "Validator.h"

#define PORT 8820

using namespace std;

class Server
{
public:
	Server();
	~Server();

	// Starts the 'handleReceivedMessages' thread and accepts new clients.
	void serve();
	string getAllUsers();
	string getOnlineUsers();

private:
	SOCKET _sock;
	mutex _mtxRecievedMessages;
	map<SOCKET, User*> _connectedUsers;
	queue <ReceivedMessage*> _queRcvMessages;
	condition_variable _cvMsgQue;
	DataBase _db;

	// Binds the socket and starts listening. If bind failed, throws an exception.
	void bindAndListen();

	// Accepts a new connection, and creates a new thread of clientHandler with their socket.
	void accept();

	// While the received message code is not QUIT or 0, builds a message with buildReceivedMessage
	// and adds it to the queue with addReceiveMessage.
	void clientHandler(SOCKET client_socket);

	User* getUserByName(string name);

	// Builds a ReceivedMessage according to the message code.
	ReceivedMessage* buildRecieveMessage(SOCKET client_socket, int msgCode);

	// Gets a message from the message queue, and sends it to one of the handle functions according to the message code.
	void handleRecievedMesssages();

	User* handleSignin(ReceivedMessage* msg);

	bool handleSignup(ReceivedMessage* msg);

	void handleSignout(ReceivedMessage* msg);

	// Adds a message to the message queue.
	void addRecievedMessages(ReceivedMessage* msg);

	void handleQuit(ReceivedMessage* msg);

	void handleFound(ReceivedMessage* msg);
};