#include "Server.h"

Server::Server()
{
	_sock = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_sock == INVALID_SOCKET)
	{
		throw exception("INVALID SOCKET");
	}
}

Server::~Server()
{
	::closesocket(_sock);
}

void Server::serve()
{
	bindAndListen();

	thread t(&Server::handleRecievedMesssages, this);
	t.detach();

	while (true)
	{
		accept();
	}
}

void Server::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	if (::bind(_sock, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw exception("EXCEPTION: SOCKET BIND");
	}

	if (::listen(_sock, SOMAXCONN) == SOCKET_ERROR)
	{
		throw exception("EXCEPTION: SOCKET LISTEN");
	}
}

void Server::accept()
{
	SOCKET client_socket = ::accept(_sock, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
	{
		throw exception("EXCEPTION: INVALID SOCKET");
	}

	cout << "Accepted new client: " << client_socket << endl;
	thread t(&Server::clientHandler, this, client_socket);
	t.detach();
}

void Server::addRecievedMessages(ReceivedMessage* msg)
{
	{
		lock_guard<mutex> lock(_mtxRecievedMessages);
		_queRcvMessages.push(msg);
	}
	_cvMsgQue.notify_all();
}

void Server::clientHandler(SOCKET client_socket)
{
	ReceivedMessage* msg = nullptr;
	try
	{
		int msgCode = Helper::getMessageTypeCode(client_socket);
		cout << "Received message code " << msgCode << " from " << client_socket << endl;
		while (msgCode != 999)
		{
			msg = buildRecieveMessage(client_socket, msgCode);
			addRecievedMessages(msg);
			msgCode = Helper::getMessageTypeCode(client_socket);
		}
		msg = buildRecieveMessage(client_socket, 999);
		addRecievedMessages(msg);
	}
	catch (exception e)
	{
		cout << e.what() << endl;
		msg = buildRecieveMessage(client_socket, 999);
		addRecievedMessages(msg);
	}
	closesocket(client_socket);
}

ReceivedMessage* Server::buildRecieveMessage(SOCKET client_socket, int msgCode)
{
	ReceivedMessage* msg = nullptr;
	vector<string> values;
	switch (msgCode)
	{
	case SIGN_IN:
	{
		int usernameLength = Helper::getIntPartFromSocket(client_socket, 2);
		string username = Helper::getStringPartFromSocket(client_socket, usernameLength);

		int passwordLength = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passwordLength);

		values.push_back(username);
		values.push_back(password);
		break;
	}
	case SIGN_UP:
	{
		int usernameLength = Helper::getIntPartFromSocket(client_socket, 2);
		string username = Helper::getStringPartFromSocket(client_socket, usernameLength);

		int passwordLength = Helper::getIntPartFromSocket(client_socket, 2);
		string password = Helper::getStringPartFromSocket(client_socket, passwordLength);

		int macAddressLength = Helper::getIntPartFromSocket(client_socket, 2);
		string macAddress = Helper::getStringPartFromSocket(client_socket, macAddressLength);

		values.push_back(username);
		values.push_back(password);
		values.push_back(macAddress);
		break;
	}
	case FOUND:
	{
		int amount = Helper::getIntPartFromSocket(client_socket, 2);
		
		for (int i = 0; i < amount; i++)
		{
			int dllNameLength = Helper::getIntPartFromSocket(client_socket, 3);
			string dllName = Helper::getStringPartFromSocket(client_socket, dllNameLength);

			values.push_back(dllName);
		}
		
		break;
	}
	default:
		msg = new ReceivedMessage(client_socket, msgCode);
		return msg;
	}
	msg = new ReceivedMessage(client_socket, msgCode, values);
	return msg;
}

void Server::handleRecievedMesssages()
{
	int msgCode = NULL;
	SOCKET client_socket = NULL;

	while (true)
	{
		unique_lock<mutex> lck(_mtxRecievedMessages);

		if (_queRcvMessages.empty())
		{
			_cvMsgQue.wait(lck);
		}

		if (_queRcvMessages.empty())
		{
			continue;
		}

		ReceivedMessage* currMessage = _queRcvMessages.front();
		_queRcvMessages.pop();
		lck.unlock();
		cout << "------------------------------" << endl;
		cout << "Handling message " << currMessage->getMessageCode() << " from " << currMessage->getSock() << endl;

		client_socket = currMessage->getSock();
		msgCode = currMessage->getMessageCode();

		switch (msgCode)
		{
		case SIGN_IN:
			handleSignin(currMessage);
			break;
		case SIGN_OUT:
			handleSignout(currMessage);
			break;
		case SIGN_UP:
			handleSignup(currMessage);
			break;
		case FOUND:
			handleFound(currMessage);
			break;
		case QUIT:
			handleQuit(currMessage);
			break;
		default:
			handleQuit(currMessage);
		}

		delete currMessage;
	}
}

bool Server::handleSignup(ReceivedMessage* msg)
{
	vector<string>& values = msg->getValues();

	if (!Validator::isPasswordValid(values[1]))
	{
		Helper::sendData(msg->getSock(), INVALID_PASSWORD);
		return false;
	}
	if (!Validator::isUsernameValid(values[0]))
	{
		Helper::sendData(msg->getSock(), INVALID_USERNAME);
		return false;
	}
	if (_db.isUserExists(values[0]))
	{
		Helper::sendData(msg->getSock(), USERNAME_ALREADY_CREATED);
		return false;
	}
	if (!_db.addNewUser(values[0], values[1], values[2]))
	{
		Helper::sendData(msg->getSock(), DATABASE_ERROR);
		return false;
	}

	Helper::sendData(msg->getSock(), CREATED_SECUSSFULLY);
	return true;
}

User* Server::handleSignin(ReceivedMessage* msg)
{
	User* newUser;
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];

	if (this->_db.isUserAndPassMatch(username, password))
	{
		if (getUserByName(username) != nullptr)
		{
			Helper::sendData(msg->getSock(), ALREADY_CONNTECTED);
			return nullptr;
		}
		Helper::sendData(msg->getSock(), CONNECTED_SECUSSFULLY);
		newUser = new User(username, msg->getSock());
		this->_connectedUsers[newUser->getSocket()] = newUser;
		return newUser;
	}

	Helper::sendData(msg->getSock(), WRONG_DETAILS);
	return nullptr;
}

User* Server::getUserByName(string name)
{
	for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != _connectedUsers.end(); ++it)
	{
		if (it->second->getUsername() == name)
		{
			return it->second;
		}
	}
	return nullptr;
}



void Server::handleSignout(ReceivedMessage* msg)
{
	if (this->_connectedUsers[msg->getSock()] != nullptr)
	{
		this->_connectedUsers.erase(msg->getSock());
	}
}

void Server::handleFound(ReceivedMessage* msg)
{
	// do something with the received dll paths...
	
	for (auto dllName : msg->getValues())
	{
		if (WhitelistedModules::global.end() == find(WhitelistedModules::global.begin(), WhitelistedModules::global.end(), dllName))
		{
			cout << "DLL Injection detected" << endl;
			break;
		}
	}

}

void Server::handleQuit(ReceivedMessage* msg)
{
	closesocket(msg->getSock());
}

string Server::getAllUsers()
{
	return this->_db.getAllUsers();
}

string Server::getOnlineUsers()
{
	string ret = "";

	for (auto& a : this->_connectedUsers)
	{
		ret += a.second->getUsername() + ";";
	}

	return ret;
}