#pragma once
#include <list>
#include <string>

namespace WhitelistedModules
{
	using namespace std;

	const list<string> global{ "ntdll.dll", "kernel32.dll", "kernelbase.dll", "ucrtbase.dll", "vcruntime140.dll", "vcruntime120.dll",
							   "msvcp140.dll", "msvcp120.dll", "msvcr120.dll"  };
}