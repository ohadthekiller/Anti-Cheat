#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve();
	}
	catch (exception& e)
	{
		cout << "Error occured: " << e.what() << endl;
	}
	system("pause");
	return 0;
}