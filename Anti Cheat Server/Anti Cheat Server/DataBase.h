#pragma once

#include "sqlite3.h"
#include <string>
#include <iostream>




#include "sqlite3.h"
#include <string>
#include <iostream>


using namespace std;

class DataBase
{
public:
	DataBase();
	~DataBase();
	bool isUserExists(string username);
	bool addNewUser(string username, string password, string macAddress);
	bool isUserAndPassMatch(string username, string password);

private:
	static int callbackCount(void* unused, int argc, char** argv, char** columns);
	sqlite3* _db;
};