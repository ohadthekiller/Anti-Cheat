#include "User.h"


User::User(std::string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
}


void User::send(std::string message)

{
	Helper::sendData(this->_sock, message);
}

std::string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}