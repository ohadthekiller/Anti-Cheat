#include "Helper.h"

#include <string>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <time.h>
#include <stdlib.h>

using namespace std;

int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	string msg(s);

	if (msg == "")
	{
		return 0;
	}

	int res = atoi(s);

	delete s;

	return  res;
}

void Helper::sendData(SOCKET sc, string message)
{
	cout << "sending to " << sc << " " + message << endl;
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{

		throw exception("Error while sending message to client");

		exception("Error while sending message to client");

	}
}

int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	
	if (s[0] == 0)
	{
		return 0;
	}

	int num = atoi(s);

	delete s;

	return num;
}

string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);

	if (s[0] == 0)
	{
		return "";
	}

	string res(s);

	delete s;

	return res;
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return "";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		string s = "Error while recieving from socket: ";
		s += to_string(sc);
		throw exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

string Helper::getPaddedNumber(int num, int digits)
{
	ostringstream ostr;

	ostr << setw(digits) << setfill('0') << num;

	ostr << std::setw(digits) << std::setfill('0') << num;

	return ostr.str();
}