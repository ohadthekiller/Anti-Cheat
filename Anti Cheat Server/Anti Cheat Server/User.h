#pragma once

#include "Helper.h"

class User
{
public:
	User(std::string username, SOCKET sock);
	void send(std::string message);//send this user message using socket
	std::string getUsername();//return string of this username
	SOCKET getSocket();//return socket of this user

private:
	std::string _username;
	SOCKET _sock;
};